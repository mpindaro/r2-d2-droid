# R2-D2 Star Wars Droid

**Questa repository è incompleta**

R2-D2 Star Wars Droid, come dice il nome, è un robottino, in plastica dura,
dall’apparenza simile al celebre robot del franchise Star Wars. Questo lavoro è stato svolgo come progetto di maturità.

Tale droide è comandato tramite un’applicazione, fruibile mediante l’installazione su
qualsiasi dispositivo Android oppure connettendosi via browser, ed è in grado di
muoversi in tutte le direzioni.

L’applicazione, oltre ad essere un controller virtuale, permette di collegarsi alla vista del
robot: esso è infatti dotato di una camera, dalla inclinazione e risoluzione regolabile,
che registra tutto ciò che vede.

L’applicazione è connessa al robot tramite hotspot Wi-Fi, configurato sullo stesso.
Quest’ultimo rende l’utilizzo del droide estremamente libero: è possibile usarlo in
qualsiasi luogo, a patto che la superficie sia piatta e che la batteria sia carica.

---

All'interno di questa repository è possibile trovare la sua documentazione (*Documentazion R2-D2.pdf),* il codice dentro alla cartella *Code* e dentro *Parts* le parti usati. 

[https://www.youtube.com/watch?v=Tt9Y_OJWLqI](https://www.youtube.com/watch?v=Tt9Y_OJWLqI)